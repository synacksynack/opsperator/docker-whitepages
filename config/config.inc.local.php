<?php
$background_image = "images/BACKGROUND_IMAGE";
$debug = DO_DEBUG;
$hover_effect = "grow";
$lang = "LANG";
$ldap_base = "LDAP_BASE";
$ldap_binddn = "LDAP_BIND_PREFIX,LDAP_BASE";
$ldap_bindpw = "LDAP_BIND_PW";
$ldap_group_base = "ou=groups,".$ldap_base;
$ldap_group_filter = "(|(objectClass=groupOfNames)(objectClass=groupOfUniqueNames)(objectClass=groupOfURLs)";
$ldap_starttls = false;
$ldap_size_limit = 100;
$ldap_url = "LDAP_PROTO://LDAP_HOST:LDAP_PORT";
$ldap_user_base = "ou=users,".$ldap_base;
$ldap_user_filter = "(objectClass=LDAP_USEROC)";
$ldap_user_regex = "/,ou=users,/i";
$logo = "images/LOGO_IMAGE";
$logout_link = "LOGOUT_LINK";

if (! $debug) { error_reporting(0); }
