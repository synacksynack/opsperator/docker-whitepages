# k8s WhitePages

LTB WhitePages image.

Based on https://ltb-project.org/documentation/white-pages

Diverts from https://gitlab.com/synacksynack/opsperator/docker-php

Build with:

```
$ make build
```

Depends on LDAP

Test with:

```
$ make run
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name              |    Description              | Default                                                     | Inherited From    |
| :---------------------------- | --------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`              | WhitePages ServerName       | `whitepages.${OPENLDAP_DOMAIN}`                             | opsperator/apache |
|  `APACHE_HTTP_PORT`           | WhitePages HTTP(s) Port     | `8080`                                                      | opsperator/apache |
|  `AUTH_METHOD`                | Apache Auth Method          | `lemon`, could be `lemon`, `ldap`, `oidc` or `none`         | opsperator/apache |
|  `BACKGROUND_IMAGE`           | WhitePages Background Image | `wall_dark.jpg` (should be in config/ building image)       |                   |
|  `LANG`                       | WhitePages Language         | `en`                                                        |                   |
|  `OIDC_CALLBACK_URL`          | OpenID Callback Path        | `/oauth2/callback`                                          |                   |
|  `OIDC_CLIENT_ID`             | OpenID Client ID            | `changeme`                                                  |                   |
|  `OIDC_CLIENT_SECRET`         | OpenID Client Secret        | `secret`                                                    |                   |
|  `OIDC_CRYPTO_SECRET`         | OpenID Crypto Secret        | `secret`                                                    |                   |
|  `OIDC_META_URL`              | OpenID Meta Path            | `/.well-known/openid-configuration`                         |                   |
|  `OIDC_PORTAL`                | OpenID Portal               | `https://auth.$OPENLDAP_DOMAIN`                             |                   |
|  `OIDC_SKIP_TLS_VERIFY`       | OpenID Skip TLS Verify      | undef                                                       |                   |
|  `OIDC_TOKEN_ENDPOINT_AUTH`   | OpenID Auth Method          | `client_secret_basic`                                       |                   |
|  `ONLY_TRUST_KUBE_CA`         | Don't trust base image CAs  | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `OPENLDAP_BASE`              | OpenLDAP Base               | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`    | OpenLDAP Bind DN Prefix     | `cn=whitepages,ou=services`                                 | opsperator/apache |
|  `OPENLDAP_BIND_PW`           | OpenLDAP Bind Password      | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX`    | OpenLDAP Conf DN Prefix     | `cn=lemonldap,ou=config`                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`            | OpenLDAP Domain Name        | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_FILTER`            | OpenLDAP Search Filters     | `(&(objectClass=$OPENLDAP_USERS_OBJECTCLASS)(active))`      |                   |
|  `OPENLDAP_HOST`              | OpenLDAP Backend Address    | `127.0.0.1`                                                 | opsperator/apache |
|  `OPENLDAP_PORT`              | OpenLDAP Bind Port          | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`             | OpenLDAP Proto              | `ldap`                                                      | opsperator/apache |
|  `OPENLDAP_SEARCH`            | OpenLDAP Search URI         | `ou=users,$OPENLDAP_BASE?uid?sub?(<objectclass>)`           |                   |
|  `OPENLDAP_USERS_OBJECTCLASS` | OpenLDAP Users ObjectClass  | `inetOrgPerson`                                             | opsperator/apache |
|  `LOGOUT_LINK`                | WhitePages Logout Link      | undef, defaults to LLNG logout URL if `AUTH_METHOD=lemon`   |                   |
|  `LOGO_IMAGE`                 | WhitePages Logo Image       | `kubelemon.png` (should be in config/ building image)       |                   |
|  `PHP_ERRORS_LOG`             | PHP Errors Logs Output      | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`     | PHP Max Execution Time      | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`       | PHP Max File Uploads        | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`          | PHP Max Post Size           | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE`    | PHP Max Upload File Size    | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`           | PHP Memory Limit            | `-1` (no limitation)                                        | opsperator/php    |
|  `PING_PATH`                  | Proxy healthcheck url       | undef - if set, `/PING_PATH` bypasses auth                  |                   |
|  `PING_ROOT`                  | Proxy healthcheck docroot   | `/var/www/html` - alternate DocumentRoot for health checks  |                   |
|  `PUBLIC_PROTO`               | WhitePages HTTP Proto       | `http`                                                      | opsperator/apache |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |
