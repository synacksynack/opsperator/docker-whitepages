FROM opsperator/php

# WhitePages image for OpenShift Origin

ARG DO_UPGRADE=
ENV LTBWP_VERSION=0.3 \
    LTBWP_REPO=https://ltb-project.org/debian/stable/pool/main


LABEL io.k8s.description="LTB WhitePages allows to browse through LDAP users and groups" \
      io.k8s.display-name="WhitePages $LTBWP_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="whitepages,ltb-whitepages,whitepages" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-whitepages" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$LTBWP_VERSION"

USER root

COPY config/* /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install WhitePages Dependencies" \
    && if test `uname -m` = aarch64; then \
	sed 's|amd64|arm64|g' /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-common php-gd php-ldap php; do \
	    echo "/." >/var/lib/dpkg/info/$dep:arm64.list; \
	done; \
    elif test `uname -m` = armv7l; then \
	sed 's|amd64|armhf|g' /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-common php-gd php-ldap php; do \
	    echo "/." >/var/lib/dpkg/info/$dep:armhf.list; \
	done; \
    else \
	cat /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-common php-gd php-ldap php; do \
	    echo "/." >/var/lib/dpkg/info/$dep:amd64.list; \
	done; \
    fi \
    && apt-get -y install --no-install-recommends smarty3 libpng16-16 \
	ldap-utils openssl libjpeg62-turbo libwebp6 libgd3 \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y install --no-install-recommends libicu-dev libwebp-dev \
	libjpeg62-turbo-dev libfreetype6-dev libpng-dev libldap2-dev gnupg2 \
	libonig-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-install mbstring intl gd ldap \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
	| awk '/=>/ { print $3 }' | sort -u | xargs -r dpkg-query -S \
	| cut -d: -f1 | sort -u | xargs -rt apt-mark manual \
    && echo "# Install WhitePages" \
    && curl -fsSL http://ltb-project.org/wiki/lib/RPM-GPG-KEY-LTB-project \
	| apt-key add - \
    && mv /ltb-project.list /etc/apt/sources.list.d/ \
    && apt-get update \
    && if ! apt-get install -y white-pages; then \
	echo WARNING: could not fetch white-pages using apt - fallback to curl \
	&& curl -fsSL -o /tmp/white-pages.deb \
	    $LTBWP_REPO/l/ltb-project-white-pages/white-pages_${LTBWP_VERSION}-1_all.deb \
	&& dpkg -i /tmp/white-pages.deb; \
    fi \
    && mkdir -p /usr/share/white-pages/htdocs/images/ \
    && mv /*.jpg /*.png /usr/share/white-pages/htdocs/images/ \
    && echo "# Enabling LDAP Modules" \
    && a2enmod ldap authnz_ldap \
    && echo "# Fixing permissions" \
    && for dir in /usr/share/white-pages/templates_c /etc/lemonldap-ng \
	/usr/share/white-pages/cache /usr/share/white-pages/conf; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root $dir \
	    && chmod -R g=u $dir; \
	done \
    && echo "# Cleaning up" \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/etc/apache2/sites-available/white-pages.conf /usr/src/php.tar.xz \
	/etc/apache2/sites-enabled/white-pages.conf /dpkg-patch \
	/etc/ldap/ldap.conf \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-whitepages.sh"]
USER 1001
